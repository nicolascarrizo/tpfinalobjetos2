package cancelacion;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import sitio.Alquiler;

public class CancelacionIntermedia implements PoliticasDeCancelacion{

	public Float saldoAPagarPorCancelacion(Alquiler alquiler) {

		float saldoAPagar = 0;
		Long diff = ChronoUnit.DAYS.between(alquiler.getFechaEntrega(), LocalDate.now());
		 
		 if(diff >= 10 && diff <= 19) {
			 saldoAPagar = alquiler.getPrecio() / 2;
		 }
		 
		 if(diff < 10) {
			 saldoAPagar = alquiler.getPrecio();
		 }
		 
		return saldoAPagar;
	}

	

}
