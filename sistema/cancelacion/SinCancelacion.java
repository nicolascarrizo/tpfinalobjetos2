package cancelacion;

import sitio.Alquiler;

public class SinCancelacion implements PoliticasDeCancelacion{

	public Float saldoAPagarPorCancelacion(Alquiler alquiler) {
		
		return alquiler.getPrecio();
	}
}
