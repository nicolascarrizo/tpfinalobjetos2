package cancelacion;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import sitio.Alquiler;

public class CancelacionGratuita implements PoliticasDeCancelacion{
	

	public Float saldoAPagarPorCancelacion(Alquiler alquiler) {
		
		 float saldoAPagar = 0;
		 Long diff = ChronoUnit.DAYS.between(alquiler.getFechaEntrega(), LocalDate.now());
		 
		 if(diff < 10) {
			 Long cantidadDeDias = ChronoUnit.DAYS.between(alquiler.getFechaEntrega(), alquiler.getFechaSalida());
			 saldoAPagar = ((alquiler.getPrecio() / cantidadDeDias.floatValue()) * 2);
		 }
		 
		 return saldoAPagar;
	
	}

}
