package cancelacion;

import sitio.Alquiler;

public interface PoliticasDeCancelacion {
	
	public Float saldoAPagarPorCancelacion(Alquiler alquiler);

}
