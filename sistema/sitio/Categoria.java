package sitio;

public class Categoria {

	private String categoriaDeRanking;

	public Categoria(String categoriaDeRanking) {
		super();
		this.categoriaDeRanking = categoriaDeRanking;
	}

	public String getCategoriaDeRanking() {
		return categoriaDeRanking;
	}
}
