package sitio;

public class TipoDeInmueble {

	private String tipoDeInmueble;

	public TipoDeInmueble(String tipoDeInmueble) {
		super();
		this.tipoDeInmueble = tipoDeInmueble;
	}

	public String getTipoDeInmueble() {
		return tipoDeInmueble;
	}
}
