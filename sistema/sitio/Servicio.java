package sitio;

public class Servicio {
	
	private String servicioDeUnInmueble;

	public Servicio(String servicioDeUnInmueble) {
		super();
		this.servicioDeUnInmueble = servicioDeUnInmueble;
	}

	public String getServicioDeUnInmueble() {
		return servicioDeUnInmueble;
	}

}
