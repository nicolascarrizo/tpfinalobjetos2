package sitio;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import filtro.BusquedaAlquiler;

public class Sitio {
	
	private Map<Usuario, LocalDate> usuarios; // map de usuario con su fecha de registro
	private Set<Alquiler> alquileres; 
	private List<TipoDeInmueble> tiposDeInmuebles;
	private Map<Usuario, List<Reserva>> reservasPorUsuario;
	private Map<String, List<Categoria>> categoriasRankeablesPorEntidad;
	private List<Servicio> serviciosDeInmuebles;

	public Sitio() {
		this.usuarios = new HashMap<Usuario, LocalDate>();
		this.alquileres = new HashSet <Alquiler>();
		this.tiposDeInmuebles = new ArrayList <TipoDeInmueble>();
		this.reservasPorUsuario = new HashMap<Usuario, List<Reserva>>();
		this.categoriasRankeablesPorEntidad= new HashMap <String, List<Categoria>>();
		this.serviciosDeInmuebles = new ArrayList<Servicio>();
	}
	
	
	public void agregarNuevoAlquiler(Alquiler alquiler, Usuario usuario) {
		
		if(this.esUnTipoDeInmuebleRegistrado(alquiler.getInmueble().getTipo()) && esUsuarioRegistrado(usuario)) {
			this.agregarCategoriasDeRankingAUnInmueblePorRegistrar(alquiler.getInmueble());
			this.agregarServiciosAUnInmueblePorRegistrar(alquiler.getInmueble());
			this.alquileres.add(alquiler);		
		}	
	}
	
	private Boolean esUsuarioRegistrado(Usuario usuario) {
		Set usuariosDelSitio = this.usuarios.keySet();
		return usuariosDelSitio.contains(usuario);
	}


	public void registrarUsuario(Usuario usuario, LocalDate fechaDeRegistro) {
		this.agregarCategoriasDeRankingAUnUsuarioPorRegistrar(usuario);
		// agregar usuario a reservas
 		usuarios.put(usuario, fechaDeRegistro);
	}
	
	/* METODO PARAMETRIZADO
	 private void agregarCategoriasDeRankingAUnaEntidadRankeablePorRegistrar(String entidadRankeable, Entidad entidad) {
		if(categoriasRankeablesPorEntidad.containsKey(entidadRankeable)) {
			List <Integer> puntajes = new ArrayList <Integer>();
	 		List <Categoria> categoriasDeEntidad = this.categoriasRankeablesPorEntidad.get(entidadRankeable);
	 		
	 		for(int i = 0; i < categoriasDeEntidad.size(); i++) {
	 			entidad.getPuntajesPorCategoria().put(categoriasDeEntidad.get(i), puntajes);
			}
		}
	}
	*/
	
	private void agregarCategoriasDeRankingAUnUsuarioPorRegistrar(Usuario usuario) {
		if(categoriasRankeablesPorEntidad.containsKey("usuario")) {
			List <Integer> puntajes = new ArrayList <Integer>();
	 		List <Categoria> categoriasDeUsuario = this.categoriasRankeablesPorEntidad.get("usuario");
	 		
	 		for(int i = 0; i < categoriasDeUsuario.size(); i++) {
	 			usuario.getPuntajesPorCategoria().put(categoriasDeUsuario.get(i), puntajes);
			}
		}
	}
	
	public void eliminarUsuario(Usuario usuario) {
		usuarios.remove(usuario);
		// eliminar usuario de reservas
	}
	
	private Boolean esUnTipoDeInmuebleRegistrado(TipoDeInmueble tipo) {
		return this.tiposDeInmuebles.contains(tipo);
	}
	
	private void agregarCategoriasDeRankingAUnInmueblePorRegistrar(Inmueble inmueble) {
		if(categoriasRankeablesPorEntidad.containsKey("inmueble")) {
			List <Integer> puntajes = new ArrayList <Integer>();
	 		List <Categoria> categoriasDeInmueble = this.categoriasRankeablesPorEntidad.get("inmueble");
			
			for(int i = 0; i < categoriasDeInmueble.size(); i++) {
				inmueble.getPuntajesPorCategoria().put(categoriasDeInmueble.get(i), puntajes);
			}
		}
	}
	
	private void agregarServiciosAUnInmueblePorRegistrar(Inmueble inmueble) {
		for(Servicio servicio: serviciosDeInmuebles) {
			inmueble.getServicios().add(servicio);
		}
	}
	
	public void registrarNuevoTipoInmueble(TipoDeInmueble tipo) {
		
		this.tiposDeInmuebles.add(tipo);
	}


	public void registrarCategoriaRankeable(String entidadRankeable, Categoria categoria) {
		
		if(this.categoriasRankeablesPorEntidad.containsKey(entidadRankeable)) {
			this.categoriasRankeablesPorEntidad.get(entidadRankeable).add(categoria);
		
		}else{
			
			List<Categoria> categorias = new ArrayList<Categoria>();
			categorias.add(categoria);
			this.categoriasRankeablesPorEntidad.put(entidadRankeable, categorias);
		}
	}
	
	public void registrarServicioDeInmueble(Servicio servicio) {
		this.serviciosDeInmuebles.add(servicio);
		
	}
	
	public void busquedaAlquiler(BusquedaAlquiler busqueda, Usuario usuario) {
		//El usuario dado por parametro es el que quiere hacer la busqueda del inmueble.
		//Verificamos si el usuario mandado por parametro existe en el Sitio para que pueda
		//hacer la busqueda.
		
		Set<Alquiler> alquileres = new HashSet<Alquiler>();	
		
		if(this.esUsuarioRegistrado(usuario)) {		
			alquileres = busqueda.filtrarBusqueda(this.alquileres);
			usuario.setAlquileresBuscados(alquileres);	
		}
		
	}
	
	public void visualizarAlquiler(Alquiler alquiler){
		visualizarInmueble(alquiler.getInmueble());
		visualizarDueño(alquiler);
	}


	private void visualizarDueño(Alquiler alquiler){
		LocalDate fechaDeRegistro = this.usuarios.get(alquiler.getPropietario());
		Integer vecesQueAlquilo  = alquiler.getInmueble().getCantVecesAlquilado();
		Integer cantDeAlquileres = this.cantDeAlquileres(alquiler.getPropietario());
		
		//falta la cant de alquileres alquilados
	}
	
	private Integer cantDeAlquileres(Usuario owner) {
		Set<Alquiler> alquileres = new HashSet <Alquiler>();
		for (Alquiler alquiler : this.alquileres) {
			if(owner == (alquiler.getPropietario())) {
				alquileres.add(alquiler);
			}
		}
		return alquileres.size();
	}


	private void visualizarInmueble(Inmueble inmueble) {
		Visualizacion visualizacion = new Visualizacion();
		visualizacion.visualizar(inmueble);
	}


	public Set<Alquiler> getAlquileres() {
		return alquileres;
	}
	
	public List<TipoDeInmueble> getTiposDeInmuebles() {
		return tiposDeInmuebles;
	}
	
	public Map<Usuario, LocalDate> getUsuarios() {
		return usuarios;
	}
	
	public Map<String, List<Categoria>> getCategoriasRankeablesPorEntidad() {
		return categoriasRankeablesPorEntidad;
	}
}
