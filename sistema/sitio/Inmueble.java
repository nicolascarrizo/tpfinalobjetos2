package sitio;


import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Inmueble extends EntidadDelSitio{
	
	private TipoDeInmueble tipo;
	private Integer superficie;
	private String pais;
	private String ciudad;
	private String direccion;
	private Integer capacidad;
	private List<Foto> fotos;
	private Set<Servicio> servicios; 
	private LocalTime horarioCheckin;
	private LocalTime horarioCkeckout;
	private List<TipoFormaDePago> tipoFormasDePago;
	private Integer cantVecesAlquilado;
	
	public Inmueble(TipoDeInmueble tipo, Integer superficie, String pais, String ciudad, String direccion, Integer capacidad,
			LocalTime horarioCheckin, LocalTime horarioCkeckout) {
		
		this.tipo = tipo;
		this.superficie = superficie;
		this.pais = pais;
		this.ciudad = ciudad;
		this.direccion = direccion;
		this.capacidad = capacidad;
		this.fotos = new ArrayList<Foto>(5);
		this.servicios = new HashSet<Servicio>();
		this.horarioCheckin = horarioCheckin;
		this.horarioCkeckout = horarioCkeckout;
		this.tipoFormasDePago = new ArrayList<TipoFormaDePago>();
		this.cantVecesAlquilado = 0;
	}

	public TipoDeInmueble getTipo() {
		return tipo;
	}

	public String getCiudad() {
		return ciudad;
	}

	public Integer getCapacidad() {
		return capacidad;
	}

	public Set<Servicio> getServicios() {
		return servicios;
	}

	public String getPais() {
		return pais;
	}

	public String getDireccion() {
		return direccion;
	}

	public List<Foto> getFotos() {
		return fotos;
	}

	public List<TipoFormaDePago> getTipoFormasDePago() {
		return tipoFormasDePago;
	}

	public LocalTime getHorarioCheckin() {
		return horarioCheckin;
	}

	public LocalTime getHorarioCkeckout() {
		return horarioCkeckout;
	}

	public Integer getSuperficie() {
		return superficie;
	}
	
	public Integer getCantVecesAlquilado() {
		return cantVecesAlquilado;
	}
}
