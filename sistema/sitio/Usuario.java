package sitio;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Usuario extends EntidadDelSitio{
	
	private String nombre;
	private String mail;
	private Integer telefono;
	private List<Alquiler> alquileres;
	private Set<Alquiler> alquileresBuscados;
	
	public Usuario(String nombre, String mail, Integer telefono) {
		super();
		this.nombre = nombre;
		this.mail = mail;
		this.telefono = telefono;
		this.alquileres = new ArrayList <Alquiler>();
		this.alquileresBuscados = new HashSet<Alquiler>();
	}

	public String getNombre() {
		return nombre;
	}
	
	public String getMail() {
		return mail;
	}

	public Integer getTelefono() {
		return telefono;
	}

	public void setAlquileresBuscados(Set<Alquiler> alquileres) {
		this.alquileresBuscados = alquileres;
	}
}
