package sitio;

import java.time.LocalDate;

import informacion.InformacionDueño;
import informacion.InformacionInmueble;
import informacion.InformacionInquilino;

public class Visualizacion {
	
	public InformacionInmueble visualizar(Inmueble inmueble) {
		InformacionInmueble info = new InformacionInmueble(inmueble);
		return info;
	}
	
	public InformacionInquilino visualizarInquilino(Usuario inquilino) {
		InformacionInquilino info = new InformacionInquilino(inquilino);
		return info;
	}
	
	public InformacionDueño visualizarDueño(Usuario dueño, LocalDate fechaRegistro, Inmueble inmueble,
			Integer cantAlquileres, Integer cantAlquileresAlquilados) {
		Integer vecesQueAlquilo  = inmueble.getCantVecesAlquilado();
		InformacionDueño info = new InformacionDueño(dueño, fechaRegistro, vecesQueAlquilo, cantAlquileres, cantAlquileresAlquilados);
		return info;
	}
}
