package sitio;

public class TipoFormaDePago {

	private String tipo;
	
	public TipoFormaDePago(String tipo) {
		super();
		this.tipo = tipo;
	}
	
	public String getTipo() {
		return this.tipo;
	}
}
