package sitio;

import java.time.LocalDate;

import cancelacion.PoliticasDeCancelacion;

public class Alquiler {
	
	private Inmueble inmueble;
	private LocalDate fechaEntrega;
	private LocalDate fechaSalida;
	private TipoFormaDePago tipoFormaDePago;
	private Float precio;
	private PoliticasDeCancelacion politicasDeCancelacion;
	private Usuario propietario;
	
	public Alquiler(Inmueble inmueble, LocalDate fechaEntrega, LocalDate fechaSalida,
			TipoFormaDePago tipoFormaDePago, Float precio, PoliticasDeCancelacion politicasDeCancelacion, Usuario propietario) {
		super();
		this.inmueble = inmueble;
		this.fechaEntrega = fechaEntrega;
		this.fechaSalida = fechaSalida;
		this.tipoFormaDePago = tipoFormaDePago;
		this.precio = precio;
		this.politicasDeCancelacion = politicasDeCancelacion;
		this.propietario = propietario;
	}

	public Inmueble getInmueble() {
		return inmueble;
	}

	public LocalDate getFechaEntrega() {
		return fechaEntrega;
	}

	public LocalDate getFechaSalida() {
		return fechaSalida;
	}

	public TipoFormaDePago getTipoFormaDePago() {
		return tipoFormaDePago;
	}

	public Float getPrecio() {
		return precio;
	}

	public PoliticasDeCancelacion getPoliticasDeCancelacion() {
		return politicasDeCancelacion;
	}

	public Usuario getPropietario() {
		return propietario;
	}
}
