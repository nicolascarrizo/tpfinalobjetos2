package sitio;

public class Foto {
	
	private Integer tama�o;
	private Integer alto;
	private Integer ancho;
	
	public Foto(Integer tama�o, Integer alto, Integer ancho) {
		super();
		this.tama�o = tama�o;
		this.alto = alto;
		this.ancho = ancho;
	}

	public Integer getTama�o() {
		return tama�o;
	}

	public Integer getAlto() {
		return alto;
	}

	public Integer getAncho() {
		return ancho;
	}
	
}
