package sitio;

public class Administrador {
	
	private Sitio sitio;

	public Administrador(Sitio sitio) {
		super();
		this.sitio = sitio;
	}
	
	
	public void altaDeTipoDeInmueble(TipoDeInmueble tipo) {
		this.sitio.registrarNuevoTipoInmueble(tipo);
	}
	
	//public void obtenerEstadosDeGestion() {}
	
	public void altaDeCategoriaDeRankingDeInmueble(Categoria categoria) {
		this.sitio.registrarCategoriaRankeable("inmueble", categoria);
	}
	
	public void altaDeCategoriaDeRankingDeUsuario(Categoria categoria) {
		this.sitio.registrarCategoriaRankeable("usuario", categoria);
	}
	
	public void altaDeServicioDeLosInmuebles(Servicio servicio) {
		this.sitio.registrarServicioDeInmueble(servicio);
	}
	
	/*private Integer cantInmueblesLibresDelSitio() {
	}*/
	
}
