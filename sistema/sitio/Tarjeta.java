package sitio;

import java.time.LocalDate;

public class Tarjeta implements FormaDePago {

	private TipoFormaDePago tipo;
	private String nombreTitular;
	private Integer numero;
	private LocalDate fechaDeCaducidad;
	private Integer codigoDeSeguridad;
	
	public Tarjeta(String nombreTitular, Integer numero, LocalDate fechaDeCaducidad, Integer codigoDeSeguridad) {
		super();
		this.tipo = new TipoFormaDePago("Tarjeta");
		this.nombreTitular = nombreTitular;
		this.numero = numero;
		this.fechaDeCaducidad = fechaDeCaducidad;
		this.codigoDeSeguridad = codigoDeSeguridad;
	}
	
	public String getTipo() {
		return tipo.getTipo();
	}
}
