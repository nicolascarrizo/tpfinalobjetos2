package sitio;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EntidadDelSitio {

	private Map<Categoria, List<Integer>> puntajesPorCategoria;
	private List<Comentario> comentarios;
	
	public EntidadDelSitio() {
		super();
		this.puntajesPorCategoria = new HashMap <Categoria, List<Integer>>();
		this.comentarios = new ArrayList<Comentario>();
	}

	public Map<Categoria, List<Integer>>  getPuntajesPorCategoria() {
		return puntajesPorCategoria;
	}
	
	public List<Comentario> getComentarios() {
		
		return this.comentarios;
	}
	
	private Integer promedioDe(List<Integer> puntajes) {
		Integer result = 0;
		 for (Integer puntaje : puntajes) {
			result+=puntaje ;
		}
		result = result / puntajes.size();
		return result;
	}

	public Map <Categoria, Integer> promedioPorCategoria(){
		Map<Categoria, Integer> promediosPorCategoria = new HashMap <Categoria,Integer>(); 
		for (Categoria key : this.puntajesPorCategoria.keySet()) {
            promediosPorCategoria.put(key, promedioDe(puntajesPorCategoria.get(key)));
       }
		return promediosPorCategoria;
	}
	
}
