package sitio;

import java.time.LocalDateTime;

public class Reserva {
	
	private Inmueble inmueble; // alquiler?
	private Usuario inquilino;
	private LocalDateTime fechaReserva;
	// formaDePago?
	
	public Reserva(Inmueble inmueble, Usuario inquilino, LocalDateTime fechaReserva) {
		super();
		this.inmueble = inmueble;
		this.inquilino = inquilino;
		this.fechaReserva = fechaReserva;
	}

	public Inmueble getInmueble() {
		return inmueble;
	}

	public Usuario getInquilino() {
		return inquilino;
	}

	public LocalDateTime getFechaReserva() {
		return fechaReserva;
	}
	
}
