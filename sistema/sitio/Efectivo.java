package sitio;

public class Efectivo implements FormaDePago {

	private TipoFormaDePago tipo;
	private Float dinero;

	public Efectivo(Float dinero) {
		super();
		this.tipo = new TipoFormaDePago("Efectivo");
		this.dinero = dinero;
	}

	public String getTipo() {
		return tipo.getTipo();
	}
}
