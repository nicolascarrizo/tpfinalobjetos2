package sitio;

public class Comentario {

	private String comentarioUsuario;

	public Comentario(String comentarioUsuario) {
		super();
		this.comentarioUsuario = comentarioUsuario;
	}

	public String getComentarioUsuario() {
		return comentarioUsuario;
	}
	
}
