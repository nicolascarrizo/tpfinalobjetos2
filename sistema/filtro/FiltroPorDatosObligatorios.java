package filtro;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import sitio.Alquiler;


public class FiltroPorDatosObligatorios extends BusquedaAlquiler{


	public FiltroPorDatosObligatorios(String ciudad, LocalDate fechaDesde, LocalDate fechaHasta) {
		super(ciudad, fechaDesde, fechaHasta);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Set<Alquiler> filtrarBusqueda(Set<Alquiler> alquileres) {
		Set<Alquiler> alquilerBuscados = new HashSet<Alquiler>();	

		for(Alquiler alquiler: alquileres) {
			if(this.datosObligatorios(alquiler)) {
				alquilerBuscados.add(alquiler);
			}
		}
		
		return alquilerBuscados;
	}

}
