package filtro;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import sitio.Alquiler;
import sitio.Sitio;

public class FiltroPorHuespedes extends BusquedaAlquiler{
	
	private Integer cantidad;
	
	public FiltroPorHuespedes(String ciudad, LocalDate fechaDesde, LocalDate fechaHasta,
			Integer cantidad) {
		super(ciudad, fechaDesde, fechaHasta);
		this.cantidad = cantidad;
	}

	@Override
	public Set<Alquiler> filtrarBusqueda(Set<Alquiler> alquileres) {
		Set<Alquiler> inmueblesBuscados = new HashSet<Alquiler>();
		
		for(Alquiler alquiler: alquileres) {
			if(alquiler.getInmueble().getCapacidad() >= this.cantidad && this.datosObligatorios(alquiler)) {
				inmueblesBuscados.add(alquiler);
			}
		}
		
		return inmueblesBuscados;
	}


}
