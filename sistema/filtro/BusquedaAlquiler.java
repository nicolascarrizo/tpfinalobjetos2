package filtro;

import java.time.LocalDate;
import java.util.Set;

import sitio.Alquiler;

public abstract class BusquedaAlquiler {
	
	private String ciudad;
	private LocalDate fechaDesde;
	private LocalDate fechaHasta;
	
	public BusquedaAlquiler(String ciudad, LocalDate fechaDesde, LocalDate fechaHasta) {
		super();
		this.ciudad = ciudad;
		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
	}

	public abstract Set<Alquiler> filtrarBusqueda(Set<Alquiler> alquileres);
	
	public Boolean datosObligatorios(Alquiler alquiler) {
		return ((alquiler.getFechaEntrega().isAfter(fechaDesde) || alquiler.getFechaEntrega().equals(fechaDesde))
				&& (alquiler.getFechaSalida().isBefore(fechaHasta) || alquiler.getFechaSalida().equals(fechaHasta)) &&
				alquiler.getInmueble().getCiudad().equals(ciudad));
	}

}
