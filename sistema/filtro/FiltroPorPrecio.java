package filtro;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import sitio.Alquiler;

public class FiltroPorPrecio extends BusquedaAlquiler{
	
	private Float precioDesde;
	private Float precioHasta;
	
	public FiltroPorPrecio(String ciudad, LocalDate fechaDesde, LocalDate fechaHasta, Float precioDesde,
			Float precioHasta) {
		super(ciudad, fechaDesde, fechaHasta);
		this.precioDesde = precioDesde;
		this.precioHasta = precioHasta;
	}

	@Override
	public Set<Alquiler> filtrarBusqueda(Set<Alquiler> alquileres) {
		Set<Alquiler> alquileresBuscados = new HashSet<Alquiler>();
		
		for(Alquiler alquiler: alquileres) {
			if(esPrecioBuscado(alquiler) && this.datosObligatorios(alquiler)) {
				alquileresBuscados.add(alquiler);
			}
		}
		
		return alquileresBuscados;
	}
	

	private Boolean esPrecioBuscado(Alquiler alquiler) {
		return (alquiler.getPrecio() >= this.precioDesde && alquiler.getPrecio() <= this.precioHasta);
	}
}
