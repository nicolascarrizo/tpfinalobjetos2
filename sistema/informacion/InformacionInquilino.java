package informacion;

import sitio.Usuario;

public class InformacionInquilino extends Informacion {
	
	private Integer telefono;
	private String mail;
	
	public InformacionInquilino(Usuario inquilino) {
		super(inquilino);
		this.telefono = inquilino.getTelefono();
		this.mail = inquilino.getMail();
	}
}
