package informacion;

import java.time.LocalDate;

import sitio.Usuario;

public class InformacionDueño extends Informacion{
	
	private LocalDate fechaDeRegistro;
	private Integer vecesQueAlquilo;
	private Integer cantDeAlquileres;
	private Integer cantAlquileresAlquilados;
	
	public InformacionDueño(Usuario dueño, LocalDate fechaDeRegistro, Integer vecesQueAlquilo,
			Integer cantDeAlquileres, Integer cantAlquileresAlquilados) {
		super(dueño);
		this.fechaDeRegistro = fechaDeRegistro;
		this.vecesQueAlquilo = vecesQueAlquilo;
		this.cantDeAlquileres = cantDeAlquileres;
		this.cantAlquileresAlquilados = cantAlquileresAlquilados;
	}
}
