package informacion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sitio.Categoria;
import sitio.Comentario;
import sitio.EntidadDelSitio;

public class Informacion  {
	private Map<Categoria, List<Integer>> puntajesPorCategoria;
	private List<Comentario> comentarios;
	private Map<Categoria, Float> promedioPorCadaCategoria;
	
	public Informacion (EntidadDelSitio entidad) {
		
		this.comentarios= entidad.getComentarios();
		this.puntajesPorCategoria= entidad.getPuntajesPorCategoria();
		this.promedioPorCadaCategoria = this.promedioPorCategoria();
	}
	
	private Float promedioDe(List<Integer> puntajes) {
		Float result = (float) 0;
		 for (Integer puntaje : puntajes) {
			result+=puntaje ;
		}
		result = result / puntajes.size();
		return result;
	}
	
	public Map <Categoria, Float> promedioPorCategoria(){
		Map<Categoria, Float> promediosPorCategoria = new HashMap <Categoria,Float>(); 
		for (Categoria key : this.puntajesPorCategoria.keySet()) {
            promediosPorCategoria.put(key, promedioDe(puntajesPorCategoria.get(key)));
		}
		return promediosPorCategoria;
	}
}
