package informacion;

import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.Set;

import sitio.Categoria;
import sitio.Comentario;
import sitio.EntidadDelSitio;
import sitio.Foto;
import sitio.Inmueble;
import sitio.Servicio;
import sitio.TipoDeInmueble;

public  class  InformacionInmueble extends Informacion{
	
	private TipoDeInmueble tipo;
	private Integer superficie;
	private String pais;
	private String ciudad;
	private String direccion;
	private Integer capacidad;
	private List<Foto> fotos;
	private Set<Servicio> servicios; 
	private LocalTime horarioCheckin;
	private LocalTime horarioCkeckout;
	
	public InformacionInmueble(Inmueble inmueble) {
		super(inmueble);
		this.tipo = inmueble.getTipo();
		this.superficie = inmueble.getSuperficie();
		this.pais= inmueble.getPais();
		this.ciudad= inmueble.getCiudad();
		this.direccion = inmueble.getDireccion();
		this.capacidad = inmueble.getCapacidad();
		this.fotos = inmueble.getFotos();
		this.servicios = inmueble.getServicios();
		this.horarioCheckin = inmueble.getHorarioCheckin();
		this.horarioCkeckout = inmueble.getHorarioCkeckout();
	}
}
