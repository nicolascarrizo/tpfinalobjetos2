package sistema;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import cancelacion.CancelacionGratuita;
import sitio.Administrador;
import sitio.Alquiler;
import sitio.Categoria;
import sitio.Inmueble;
import sitio.Sitio;
import sitio.TipoDeInmueble;
import sitio.TipoFormaDePago;
import sitio.Usuario;

public class SistemaDeAlquileresTestCase {

	private Administrador admin;
	private Sitio sitio;
	private Usuario homero;
	private Usuario marge;
	private Usuario bart;
	private TipoDeInmueble ph;
	private TipoDeInmueble casa;
	private TipoDeInmueble departamento;
	private Categoria limpieza;
	private Categoria orden;
	private Categoria ubicacion;
	private Inmueble casaDeHomero;
	private Inmueble depto1;
	private Alquiler alquilerCasaDeHomero1;
	private Alquiler alquilerDepto11;
	private CancelacionGratuita cg;
	
	@BeforeEach
	public void setUp() throws Exception {
		sitio = new Sitio();
		admin = new Administrador(sitio);
		homero = new Usuario("Homero Simpson", "homero@gmail.com", 1147859632);
		marge = new Usuario("Marge Simpson", "marge@gmail.com", 1126584132);
		bart = new Usuario("Bart Simpson", "bart@gmail.com", 1186325956);
		ph = new TipoDeInmueble("PH");
		casa = new TipoDeInmueble("Casa");
		departamento = new TipoDeInmueble("Departamento");
		limpieza = new Categoria("Limpieza");
		orden = new Categoria("Orden");
		ubicacion = new Categoria("Ubicación");
		casaDeHomero = new Inmueble(casa, 400, "Estados Unidos", "Springfield", 
				"Calle Siempre Viva 742", 5, LocalTime.of(9, 00), LocalTime.of(18, 00));
		depto1 = new Inmueble(departamento, 200, "Argentina", "Quilmes", 
				"Calle Rivadavia 845", 3, LocalTime.of(7, 00), LocalTime.of(21, 00));
		TipoFormaDePago tipoTarjeta = new TipoFormaDePago("Tarjeta");
		alquilerCasaDeHomero1 = new Alquiler(casaDeHomero, LocalDate.of(2021, 7, 8), 
				LocalDate.of(2021, 7, 20), tipoTarjeta, (float) 14000, cg, homero);
		alquilerDepto11 = new Alquiler(depto1, LocalDate.of(2021, 7, 2), 
				LocalDate.of(2021, 7, 10), tipoTarjeta, (float) 12500, cg, bart);
	}

	//------------------------------------------------------------------------------------------
	//                               TESTS DE SITIO
	
	@Test
	public void testElSitioNoTieneTiposDeInmuebles() {
		assertTrue(sitio.getTiposDeInmuebles().isEmpty());
	}
	
	@Test
	public void testElAdminAgrega3TiposDeInmueblesAlSitio() {
		admin.altaDeTipoDeInmueble(ph);
		admin.altaDeTipoDeInmueble(casa);
		admin.altaDeTipoDeInmueble(departamento);
		assertEquals(3, sitio.getTiposDeInmuebles().size());
	}

	@Test
	public void testElSitioNotieneUsuariosRegistrados() {
		assertTrue(sitio.getUsuarios().isEmpty());
	}
	
	@Test
	public void testSeRegistran2UsuariosAlSitio() {
		sitio.registrarUsuario(homero, LocalDate.of(2021, 7, 8));
		sitio.registrarUsuario(marge, LocalDate.of(2021, 7, 8));
		sitio.registrarUsuario(homero, LocalDate.of(2021, 7, 8));
		assertEquals(2, sitio.getUsuarios().size());
	}
	
	@Test
	public void testSeRegistran3UsuariosAlSitioSinRepetir() {
		sitio.registrarUsuario(homero, LocalDate.of(2021, 7, 8));
		sitio.registrarUsuario(marge, LocalDate.of(2021, 7, 8));
		sitio.registrarUsuario(bart, LocalDate.of(2021, 7, 8));
		assertEquals(3, sitio.getUsuarios().size());
	}
	
	@Test
	public void testSeRegistraUnUsuarioAlSitioYLuegoSeElimina() {
		sitio.registrarUsuario(homero, LocalDate.of(2021, 7, 8));
		sitio.eliminarUsuario(homero);
		assertTrue(sitio.getUsuarios().isEmpty());
	}
	
	@Test
	public void testElSitioNoTieneCategoriasDeRankingDeUsuarios() {
		assertFalse(sitio.getCategoriasRankeablesPorEntidad().containsKey("usuario"));
	}
	
	@Test
	public void testElAdminAgrega2CategoriasDeRankingDeUsuariosAlSitio() {
		admin.altaDeCategoriaDeRankingDeUsuario(limpieza);
		admin.altaDeCategoriaDeRankingDeUsuario(orden);
		assertEquals(2, sitio.getCategoriasRankeablesPorEntidad().get("usuario").size());
	}
	
	@Test
	public void testElSitioNotieneAlquileresRegistrados() {
		assertTrue(sitio.getAlquileres().isEmpty());
	}
	
	@Test
	public void testNoSeAgregaUnAlquilerAlSitioSiElTipoDeInmuebleDelInmuebleNoSeDioDeAlta() {
		sitio.registrarUsuario(homero, LocalDate.of(2021, 7, 8));
		sitio.agregarNuevoAlquiler(alquilerCasaDeHomero1, homero);
		assertTrue(sitio.getAlquileres().isEmpty());
	}
	
	@Test
	public void testNoSeAgregaUnAlquilerAlSitioSiElUsuarioDadoNoEstaRegistradoEnElMismo() {
		admin.altaDeTipoDeInmueble(casa);
		sitio.agregarNuevoAlquiler(alquilerCasaDeHomero1, homero);
		assertTrue(sitio.getAlquileres().isEmpty());
	}
	
	@Test
	public void testSeAgregan2AlquileresAlSitio() {
		sitio.registrarUsuario(homero, LocalDate.of(2021, 7, 8));
		sitio.registrarUsuario(bart, LocalDate.of(2021, 7, 8));
		admin.altaDeTipoDeInmueble(casa);
		admin.altaDeTipoDeInmueble(departamento);
		sitio.agregarNuevoAlquiler(alquilerCasaDeHomero1, homero);
		sitio.agregarNuevoAlquiler(alquilerDepto11, bart);
		sitio.agregarNuevoAlquiler(alquilerCasaDeHomero1, homero);
		assertEquals(2, sitio.getAlquileres().size());
	}
	
	@Test
	public void testSeAgregan2AlquileresAlSitioSinRepetir() {
		sitio.registrarUsuario(homero, LocalDate.of(2021, 7, 8));
		sitio.registrarUsuario(bart, LocalDate.of(2021, 7, 8));
		admin.altaDeTipoDeInmueble(casa);
		admin.altaDeTipoDeInmueble(departamento);
		sitio.agregarNuevoAlquiler(alquilerCasaDeHomero1, homero);
		sitio.agregarNuevoAlquiler(alquilerDepto11, bart);
		assertEquals(2, sitio.getAlquileres().size());
	}
	
	@Test
	public void testElSitioNoTieneCategoriasDeRankingDeInmuebles() {
		assertFalse(sitio.getCategoriasRankeablesPorEntidad().containsKey("inmueble"));
	}
	
	@Test
	public void testElAdminAgrega2CategoriasDeRankingDeInmueblesAlSitio() {
		admin.altaDeCategoriaDeRankingDeInmueble(limpieza);
		admin.altaDeCategoriaDeRankingDeInmueble(ubicacion);
		assertEquals(2, sitio.getCategoriasRankeablesPorEntidad().get("inmueble").size());
	}
	
	//------------------------------------------------------------------------------------------
	//                                 TESTS DE USUARIO
	
	@Test
	public void testUnUsuarioRecienAgregadoAlSitioTiene2CategoriasDeRanking() {
		admin.altaDeCategoriaDeRankingDeUsuario(limpieza);
		admin.altaDeCategoriaDeRankingDeUsuario(orden);
		sitio.registrarUsuario(homero, LocalDate.of(2021, 7, 8));
		assertEquals(2, homero.getPuntajesPorCategoria().keySet().size());
	}
	
	@Test
	public void testUnUsuarioAgregadoAUnSitioSinCategoriasDeRankingDeUsuarioNoTienePuntajes() {
		sitio.registrarUsuario(homero, LocalDate.of(2021, 7, 8));
		assertTrue(homero.getPuntajesPorCategoria().isEmpty());
	}
	
	//------------------------------------------------------------------------------------------
	//                                TESTS DE INMUEBLE

	@Test
	public void testUnInmuebleRecienAgregadoAlSitioTiene2CategoriasDeRanking() {
		admin.altaDeCategoriaDeRankingDeInmueble(limpieza);
		admin.altaDeCategoriaDeRankingDeInmueble(ubicacion);
		admin.altaDeTipoDeInmueble(casa);
		sitio.registrarUsuario(homero, LocalDate.of(2021, 7, 8));
		sitio.agregarNuevoAlquiler(alquilerCasaDeHomero1, homero);
		assertEquals(2, casaDeHomero.getPuntajesPorCategoria().keySet().size());
	}
	
	@Test
	public void testUnInmuebleAgregadoAUnSitioSinCategoriasDeRankingDeInmuebleNoTienePuntajes() {
		admin.altaDeTipoDeInmueble(casa);
		sitio.registrarUsuario(homero, LocalDate.of(2021, 7, 8));
		sitio.agregarNuevoAlquiler(alquilerCasaDeHomero1, homero);
		assertTrue(casaDeHomero.getPuntajesPorCategoria().isEmpty());
	}
}
